// ns = north south
// ew = east west
int red_ns = 13;
int yellow_ns = 12;
int green_ns = 11; 

int red_ew = 7;
int yellow_ew = 6;
int green_ew = 5; 

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(red_ns, HIGH);             
  digitalWrite(yellow_ns, LOW);
  digitalWrite(green_ns, LOW);
  digitalWrite(red_ew, LOW); 
  digitalWrite(yellow_ew, LOW);
  digitalWrite(green_ew, HIGH);
  
  delay(1000);                      

  digitalWrite(red_ns, LOW);
  digitalWrite(yellow_ns, HIGH);
  digitalWrite(red_ew, LOW);
  digitalWrite(yellow_ew, HIGH);
  digitalWrite(green_ew, LOW);
  delay(1000);

  digitalWrite(yellow_ns, LOW);
  digitalWrite(green_ns, HIGH);
  digitalWrite(red_ew, HIGH);             
  digitalWrite(yellow_ew, LOW);
  digitalWrite(green_ew, LOW);
  delay(1000);
  
}

