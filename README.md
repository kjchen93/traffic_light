# Traffic Light

Simulation of embedded traffic light. light switches from each second. The lights are in sync such that traffic traveling from east to west does not collide with the traffic from north to south. Images below shows the states of the traffic lights


![Red North/South Green East/West](./imgs/traficlight1.jpg)

![Yellow](./imgs/traficlight2.jpg)

![Red East/West, Green North/South](./imgs/traficlight3.jpg)

![](./imgs/source_code.jpg)

## Author

[Kai Chen](gitlab.com/kjchen93)